SELECT DISTINCT function, ROUND(AVG(salary) OVER (PARTITION BY job_id), 2) AS average_salary
FROM employee
INNER JOIN job
	USING(job_id)
ORDER BY average_salary DESC;