SELECT function, salary 
FROM employee 
INNER JOIN job 
	USING(job_id)
ORDER BY function;