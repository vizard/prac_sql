SELECT * 
FROM employee
INNER JOIN department 
	USING(department_id)
FULL JOIN location
	ON false
ORDER BY hire_date;