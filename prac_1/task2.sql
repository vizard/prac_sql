--SELECT DISTINCT city, COUNT(*) OVER (PARTITION BY city) AS customers_num
SELECT DISTINCT city, COUNT(*) OVER (PARTITION BY city, location_id) AS customers_num
FROM customer
	INNER JOIN location 
		ON UPPER(regional_group) = UPPER(city)
--WHERE
--	EXISTS
--		(SELECT department_id FROM department 
--		 	WHERE location_ID = department.location_id); -- dep. existance check