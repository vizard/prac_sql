SELECT SUM(item.total) - SUM(min_price * quantity) AS total_income
FROM item
INNER JOIN product 
	USING(product_id)
INNER JOIN price
	USING(product_id)
INNER JOIN sales_order 
	USING(order_ID)
WHERE (description = 'ACE TENNIS NET') AND (order_date >= start_date) AND (end_date IS null OR order_date <= end_date);