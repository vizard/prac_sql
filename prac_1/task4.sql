SELECT employee_id, last_name, first_name, hire_date, regional_group, MIN(hire_date) OVER (PARTITION BY location_id) AS min_hire 
FROM employee
INNER JOIN department 
	USING(department_id)
INNER JOIN location
	USING(location_id)
WHERE  hire_date = min_hire AND UPPER(regional_group) = 'DALLAS'; --add windowed function to where clause
--		AND hire_date = (SELECT MIN(hire_date) FROM employee) --var2
-- ORDER BY hire_date ASC									  --var1
-- LIMIT 1 OFFSET 0;